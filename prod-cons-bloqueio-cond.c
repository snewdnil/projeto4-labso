/*----------------------------------
 * Laboratório de Sistemas Operacionais
 * Projeto 4: Programação Concorrente
 * Nome: Henrique Teruo Eihara          RA: 490016
 * Nome: Marcello da Costa Marques Acar RA: 552550
----------------------------------- */
#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <crypt.h>

#define N_ITENS 100
#define TAM_SENHA 4
#define TAM_HASH 256
#define NUM_PRODUTOR 1
#define NUM_CONSUMIDOR 4

char * senhaAlvo;
char * buffer[N_ITENS];
char senha[TAM_SENHA] = "aaaa";
char * auxiliar;

int inicio = 0, final = 0, cont = 0, encontrada = 0, finalizada = 0;

pthread_mutex_t bloqueio;
pthread_cond_t nao_vazio, nao_cheio, encontrado;

int incrementa_senha() {
  int i;

  i = TAM_SENHA - 1;
  while (i >= 0) {
    if (senha[i] != 'z') {
      senha[i]++;
      i = -2;
    } else {
      senha[i] = 'a';
      i--;
    }
  }
  if (i == -1) {
    return 0; // quando não há mais como incrementar
  }else{
    return 1;
  }
}
void calcula_hash_senha(const char *senha, char *hash) {
  struct crypt_data data;
  data.initialized = 0;
  strcpy(hash, crypt_r(senha, "aa", &data));
}

int testa_senha(const char *hash_alvo, const char *senha) {
  char hash_calculado[TAM_HASH + 1];

  calcula_hash_senha(senha, hash_calculado);
  if (!strcmp(hash_alvo, hash_calculado)) {
    return 0;
  }
  return 1;
}

void * produtor(void *v) {
  do{
    // bloqueia o buffer para que o produtor
    // possa gerar mais senhas
    pthread_mutex_lock(&bloqueio);

    // caso o buffer esteja cheio, esperara o sinal
    // quando a thread do consumidor consumir.
    // caso a chave seja encontrada, ela esperara o sinal
    // e mandara também um sinal para as threads consumidoras,
    // assim a produtora e as consumidoras podem fechar as threads
    while (cont == N_ITENS) {
      pthread_cond_wait(&nao_cheio, &bloqueio);
      if(encontrada){
        pthread_cond_wait(&encontrado, &bloqueio);
        pthread_cond_signal(&nao_vazio);
        pthread_mutex_unlock(&bloqueio);
        return NULL;
      }
    }

    // local aonde se gera as chaves
    auxiliar = (char*)malloc(sizeof(char)*TAM_SENHA);

    // guarda as chaves em auxiliar
    strcpy(auxiliar, senha);

    // limita o buffer para que não ultrapasse
    // o limite decidido pelos #defines
    final = (final + 1) % N_ITENS;

    // guarda o endereço de auxiliar
    buffer[final] = auxiliar;

    // quando é armazenado a nova chave
    // no contador é adicionado mais um
    cont += 1;

    // manda um sinal para as threads consumidoras
    // para que elas possam consumir as chaves
    pthread_cond_signal(&nao_vazio);

    // desbloqueia o buffer
    pthread_mutex_unlock(&bloqueio);
  }while(incrementa_senha());

  // caso todas as senhas forem geradas
  // e não derem certo a verificação, 
  // o produtor apenas sairá
  finalizada = 1;

  // certifica que todas as senhas geradas
  // foram verificadas e espera as threads
  // consumidoras se destruirem.
  // é enviado o sinal nao_vazio, pois como
  // o produtor parou de produzir senhas, o cont
  // ficará como zero, e no produtor, ele acabará
  // sempre entrando no while de (cont == 0)
  do{
    pthread_cond_signal(&nao_vazio);
    pthread_mutex_unlock(&bloqueio);
  }while(finalizada < 1 + NUM_CONSUMIDOR);

  if(!encontrada)
    printf("Senha não encontrada...\n");

  return NULL;
}

void* consumidor(void *v) {
  // variavel local para que a thread
  // possa trabalhar com ela independentemnete
  // do buffer
  char * variavelLocal = NULL;

  do{
    free(variavelLocal);

    // bloqueia o acesso de outras threads
    // no buffer
    pthread_mutex_lock(&bloqueio);

    // caso o buffer esteja vazio, esperará até
    // que o produtor produza algo.
    // caso já não tenho mais senhas para verificar
    // e o produtor ja tiver finalizado, o consumidor
    // irá enviar um sinal de finalizado e adicionará
    // na variaǘel global finalizada para que o produtor
    // saiba quantas threads já foram destruídas.
    while (cont == 0) {
      pthread_cond_wait(&nao_vazio, &bloqueio);
      if(finalizada > 0){
        finalizada++;
        return NULL;
      }
    }

    // caso seja encontrada a senha, essa thread
    // enviará um sinal para o produtor para que
    // encerre a produção, no final, a thread
    // é destruída.
    if(encontrada){
      pthread_cond_signal(&nao_cheio);
      pthread_cond_signal(&encontrado);
      pthread_mutex_unlock(&bloqueio);
      return NULL;
    }

    // permite apenas o acesso do buffer
    // limitado pelo #define
    inicio = (inicio + 1) % N_ITENS;
    variavelLocal = buffer[inicio];

    // diminui o número de chaves armazenadas
    // no buffer
    cont -= 1;

    // desbloqueia para que produtor crie mais
    // chaves
    pthread_cond_signal(&nao_cheio);
    pthread_mutex_unlock(&bloqueio);
  }while(testa_senha(senhaAlvo, variavelLocal));

  // caso encontre a chave, é definido em uma variávle
  // global encontrada = 1, para que as outras threads
  // possam ter conhecimento
  encontrada = 1;

  printf("Senha encontrada: %s\n",variavelLocal);

  return NULL;
}

int main(int argc, char *argv[]) {
  int i;

  // verifica o número de argumentos
  if(argc != 2){
    printf("Usage: %s <hash>\n",argv[0]);
    exit(0);
  }

  senhaAlvo = (char*)malloc(sizeof(char)*(strlen(argv[1])));
  strcpy(senhaAlvo,argv[1]);

  // threads de produtor e consumidor respectivamente
  pthread_t thr_produtor[NUM_PRODUTOR], thr_consumidor[NUM_CONSUMIDOR];

  // cria os estados e o mutex
  pthread_mutex_init(&bloqueio, NULL);
  pthread_cond_init(&nao_cheio, NULL);
  pthread_cond_init(&nao_vazio, NULL);
  pthread_cond_init(&encontrado, NULL);

  // cria as threads
  for(i = 0; i < NUM_PRODUTOR; i++)
    pthread_create(&thr_produtor[i], NULL, produtor, NULL);
  for(i = 0; i < NUM_CONSUMIDOR; i++)
    pthread_create(&thr_consumidor[i], NULL, consumidor, NULL);

  // finaliza as threads
  for(i=0; i < NUM_PRODUTOR; i++)
    pthread_join(thr_produtor[i], NULL);
  for(i=0; i < NUM_CONSUMIDOR; i++)
    pthread_join(thr_consumidor[i], NULL);

  // limpa o buffer e a senha
  free(senhaAlvo);
  free(*buffer);

  return 0;
}
